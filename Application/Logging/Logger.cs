﻿using System;
using Logging.Entities;
using Logging.Infrastructure.Database;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Logging
{
    internal class Logger : ILogger
    {
        public bool IsStructured { get; set; }
        
        private readonly string _categoryName;
        private readonly IServiceProvider _serviceProvider;

        public Logger(IServiceProvider serviceProvider, string categoryName)
        {
            _serviceProvider = serviceProvider;
            _categoryName = categoryName;
        }
        
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            try
            {
                var entry = new LogEntry
                {
                    LogLevel = logLevel,
                    Data = formatter.Invoke(state, exception),
                    Event = eventId.ToString(),
                    OccurredAt = DateTime.Now,
                    Category = _categoryName
                };

                using var scope = _serviceProvider.CreateScope();
                using var context = scope.ServiceProvider.GetRequiredService<ILoggingContext>();
                
                context.Logs.Add(entry);
                context.SaveChanges();

                IsStructured = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public bool IsEnabled(LogLevel logLevel) => logLevel != LogLevel.None;
        public IDisposable BeginScope<TState>(TState state) => default;
    }
}