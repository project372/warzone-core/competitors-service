﻿using System;
using System.Text.Json;
using Logging.Entities;
using Microsoft.Extensions.Logging;

namespace Logging
{
    public static class LoggerExtensions
    {
        public static void LogStructuredException(this ILogger logger, Exception exception, object data)
        {
            object dataToRecord = new
            {
                exceptionType = exception.GetType().FullName,
                exceptionMessage = exception.Message,
                exceptionStackTrace = exception.StackTrace,
                data
            };
            logger.LogError(exception, JsonSerializer.Serialize(dataToRecord));
        }
    }
}