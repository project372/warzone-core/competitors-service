﻿using System;
using Microsoft.Extensions.Logging;

namespace Logging.Entities
{
    public class LogEntry
    {
        public int Id { get; set; }
        public LogLevel LogLevel { get; set; }
        public string Event { get; set; }
        public string Data { get; set; }
        public string Category { get; set; }
        public DateTime OccurredAt { get; set; }
    }
}