﻿using System;
using Logging.Infrastructure.Database;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Logging
{
    public static class DependencyInjection
    {
        public static ILoggingBuilder AddCustomLoggerProvider(this ILoggingBuilder loggingBuilder)
        {
            loggingBuilder.Services.AddSingleton<ILoggerProvider, CustomLoggerProvider>();

            return loggingBuilder;
        }
        
    }
}