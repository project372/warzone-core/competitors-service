﻿using System;
using System.Threading.Tasks;
using Logging.Entities;
using Microsoft.EntityFrameworkCore;

namespace Logging.Infrastructure.Database
{
    public interface ILoggingContext : IDisposable
    {
        DbSet<LogEntry> Logs { get; set; }

        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}