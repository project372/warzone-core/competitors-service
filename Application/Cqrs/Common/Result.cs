﻿using System.Collections.Generic;
using Cqrs.Common.Enums;
using Entities.Common;

namespace Cqrs.Common
{
    public record Result
    {
        public ResultType Type { get; init; } = ResultType.Success;
        public IEnumerable<string> ValidationErrors { get; init; }
    }

    public record Result<T> : Result
    {
        public T Data { get; init; }
    }
}