﻿namespace Cqrs.Common.Enums
{
    public enum ResultType
    {
        Success,
        InternalError,
        NotValid,
        NotFound
    }
}