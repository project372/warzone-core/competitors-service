﻿using System.Threading.Tasks;
using Cqrs.Common;

namespace Cqrs.Queries.Interfaces
{
    public interface IGetPersonQuery
    {
        Task<Result> InvokeAsync(int id);
        Task<Result> InvokeAsync(string cpf);
    }
}