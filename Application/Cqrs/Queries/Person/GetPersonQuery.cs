﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cqrs.Common;
using Cqrs.Common.Enums;
using Cqrs.Infrastructure.Database;
using Cqrs.Queries.Interfaces;
using Entities.ValueObjects.Person;
using Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Cqrs.Queries.Person
{
    public class GetPersonQuery : IGetPersonQuery
    {

        private readonly ILogger<GetPersonQuery> _logger;
        private readonly IContext _context;

        public GetPersonQuery(ILogger<GetPersonQuery> logger, IContext context)
        {
            _logger = logger;
            _context = context;
        }


        public async Task<Result> InvokeAsync(int id)
        {
            try
            {
                return await GetPersonByIdAsync(id);
            }
            catch (Exception e)
            {
                return ReturnInternalError(e, new {id});
            }
        }
        
        public async Task<Result> InvokeAsync(string cpf)
        {
            try
            {
                return await GetPersonByCpfAsync(cpf);
            }
            catch (Exception e)
            {
                return ReturnInternalError(e, new{cpf});
            }
        }

        private Result ReturnInternalError(Exception e, object data)
        {
            _logger?.LogStructuredException(e, data);
            return new Result
            {
                Type = ResultType.InternalError
            };
        }

        private async Task<Result> GetPersonByIdAsync(int id)
        {
            var query = _context.Persons.Where(x => x.Id == id);
            return await GetPersonResult(query);
        }

        private async Task<Result> GetPersonByCpfAsync(string cpf)
        {
            var queryCpf = new Cpf {Value = cpf};
            var query = _context.Persons.Where(x => x.CpfNumber == queryCpf);
            return await GetPersonResult(query);
        }

        private async Task<Result> GetPersonResult(IQueryable<Entities.Entities.Person> queryable)
        {
            var person = await queryable
                .Include(x => x.BankAccount)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            
            if (person is null)
                return new Result
                {
                    Type = ResultType.NotFound
                };

            return new Result<Entities.Entities.Person>
            {
                Data = person
            };
        }
    }
}