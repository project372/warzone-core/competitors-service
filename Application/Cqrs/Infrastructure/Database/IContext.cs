﻿using System;
using System.Threading.Tasks;
using Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cqrs.Infrastructure.Database
{
    public interface IContext : IDisposable
    {
        DbSet<Person> Persons { get; set; }
        DbSet<Documents> Documents { get; set; }
        DbSet<BankAccount> BankAccounts { get; set; }
        DbSet<Country> Countries { get; set; }

        Task<int> SaveChangesAsync();
        int SaveChanges();

    }
}