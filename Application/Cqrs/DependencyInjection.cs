﻿using Cqrs.Commands.Interfaces;
using Cqrs.Commands.Interfaces.Person;
using Cqrs.Commands.Person;
using Cqrs.Queries.Interfaces;
using Cqrs.Queries.Person;
using Microsoft.Extensions.DependencyInjection;

namespace Cqrs
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommandsAndQueries(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISavePersonCommand, SavePersonCommand>();

            serviceCollection.AddTransient<IGetPersonQuery, GetPersonQuery>();

            return serviceCollection;
        }
    }
}