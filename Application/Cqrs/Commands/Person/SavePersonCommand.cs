﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cqrs.Commands.Interfaces;
using Cqrs.Commands.Interfaces.Person;
using Cqrs.Common;
using Cqrs.Common.Enums;
using Cqrs.Infrastructure.Database;
using Entities.Common.Validation;
using Logging;
using Microsoft.Extensions.Logging;

namespace Cqrs.Commands.Person
{
    internal class SavePersonCommand : ISavePersonCommand
    {
        private readonly IContext _context;
        private readonly ILogger<SavePersonCommand> _logger;

        public SavePersonCommand(IContext context, ILogger<SavePersonCommand> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> InvokeAsync(Entities.Entities.Person person)
        {
            try
            {
                var (isValid, errors) = person.Validate();
                if (!isValid)
                    return new Result
                    {
                        Type = ResultType.NotValid,
                        ValidationErrors = errors
                    };

                return person.Id == default ? await AddPersonAsync(person) : await UpdatePersonAsync(person);
            }
            catch (Exception e)
            { 
                _logger?.LogStructuredException(e, person);
                return new Result
                {
                    Type = ResultType.InternalError,
                };
            } 
        }

        private async Task<Result> UpdatePersonAsync(Entities.Entities.Person person)
        {
            _context.Persons.Update(person);
            await _context.SaveChangesAsync();

            return new Result<Entities.Entities.Person>
            {
                Data = person
            };
        }

        private async Task<Result> AddPersonAsync(Entities.Entities.Person person)
        {
            var repeatedPersonsErrors = CheckForRepeatedEmailCpfOrIdentity(person);

            if (repeatedPersonsErrors.Any())
                return new Result
                {
                    Type = ResultType.NotValid,
                    ValidationErrors = repeatedPersonsErrors
                };

            _context.Persons.Add(person);
            await _context.SaveChangesAsync();

            return new Result<Entities.Entities.Person>()
            {
                Data = person
            };
        }

        private List<string> CheckForRepeatedEmailCpfOrIdentity(Entities.Entities.Person person)
        {
            var repeatedPersons = _context.Persons.Where(
                x => x.Email == person.Email ||
                     x.CpfNumber == person.CpfNumber ||
                     x.IdentityNumber == person.IdentityNumber);

            var repeatedPersonsErrors = new List<string>();

            foreach (var repeatedPerson in repeatedPersons)
            {
                if (repeatedPerson.Email == person.Email && repeatedPersonsErrors.All(x => x != PersonValidation.RepeatedEmail))
                    repeatedPersonsErrors.Add(PersonValidation.RepeatedEmail);

                if (repeatedPerson.CpfNumber == person.CpfNumber && repeatedPersonsErrors.All(x => x != PersonValidation.RepeatedCpf))
                    repeatedPersonsErrors.Add(PersonValidation.RepeatedCpf);

                if (repeatedPerson.IdentityNumber == person.IdentityNumber && repeatedPersonsErrors.All(x => x != PersonValidation.RepeatedIdentity))
                    repeatedPersonsErrors.Add(PersonValidation.RepeatedIdentity);
            }

            return repeatedPersonsErrors;
        }
    }
}