﻿using System.Threading.Tasks;
using Cqrs.Common;

namespace Cqrs.Commands.Interfaces.Person
{
    public interface ISavePersonCommand
    {
        Task<Result> InvokeAsync(Entities.Entities.Person person);        
    }
}