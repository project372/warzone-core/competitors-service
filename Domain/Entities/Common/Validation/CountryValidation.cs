﻿namespace Entities.Common.Validation
{
    public class CountryValidation
    {
        public static readonly string EmptyCountryName = nameof(EmptyCountryName);
        public static readonly string EmptyLegalAge = nameof(EmptyLegalAge);
    }
}