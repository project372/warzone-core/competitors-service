﻿using System.Collections.Generic;

namespace Entities.Common.Validation
{
    public class BankAccountValidation
    {
        public static string EmptyAccountNumber = nameof(EmptyAccountNumber);
        public static string InvalidAccountNumber = nameof(InvalidAccountNumber);
        
        public static string EmptyAccountDigit = nameof(EmptyAccountDigit);
        public static string InvalidAccountDigit = nameof(InvalidAccountDigit);

        public static string EmptyBankName = nameof(EmptyBankName);
        
        public static string EmptyBankAgency = nameof(EmptyBankAgency);
        public static string InvalidBankAgency = nameof(InvalidBankAgency);
    }
}