﻿using System.Collections;
using System.Collections.Generic;

namespace Entities.Common.Validation
{
    public class PersonValidation
    {
        public static readonly string InvalidCpf  = nameof(InvalidCpf);
        public static readonly string EmptyCpf  = nameof(EmptyCpf);
        public static readonly string EmptyFirstName  = nameof(EmptyFirstName);
        public static readonly string EmptyLastName  = nameof(EmptyLastName);
        public static readonly string EmptyCountryCode  = nameof(EmptyCountryCode);
        public static readonly string InvalidCountryCode  = nameof(InvalidCountryCode);
        public static readonly string EmptyAreaCode  = nameof(EmptyAreaCode);
        public static readonly string InvalidAreaCode  = nameof(InvalidAreaCode);
        public static readonly string EmptyPhoneNumber  = nameof(EmptyPhoneNumber);
        public static readonly string InvalidPhoneNumber  = nameof(InvalidPhoneNumber);
        public static readonly string InvalidEmail  = nameof(InvalidEmail);
        public static readonly string EmptyEmail  = nameof(EmptyEmail);
        public static readonly string EmptyAddress  = nameof(EmptyAddress);
        public static readonly string EmptyState  = nameof(EmptyState);
        public static readonly string EmptyZipCode  = nameof(EmptyZipCode);
        public static readonly string InvalidZipCode  = nameof(InvalidZipCode);
        public static readonly string InvalidIdentityNumber  = nameof(InvalidIdentityNumber);
        public static readonly string EmptyCountry  = nameof(EmptyCountry);
        public static readonly string EmptyIdentityNumber  = nameof(EmptyIdentityNumber);

        public static readonly string RepeatedEmail = nameof(RepeatedEmail);
        public static readonly string RepeatedCpf = nameof(RepeatedCpf);
        public static readonly string RepeatedIdentity = nameof(RepeatedIdentity);
    }
}