﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Entities.Common.Interfaces;

namespace Entities.Common.Validation
{
    public abstract record RequiredOnlyNumbersString : IValidatable
    {
        private readonly string _emptyFieldValidationError;
        private readonly string _invalidFieldValidationError;

        protected RequiredOnlyNumbersString(string emptyFieldValidationError, string invalidFieldValidationError)
        {
            _invalidFieldValidationError = invalidFieldValidationError;
            _emptyFieldValidationError = emptyFieldValidationError;
        }


        public string Value { get; init; }
        
        protected bool IsOnlyNumbers() => Regex.IsMatch(Value, "^[0-9]*$");
        protected int[] GetAsNumberEnumerable() => Value.Select(c => int.Parse(c.ToString())).ToArray();

        public virtual (bool isValid, IEnumerable<string> errors) Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return (false, new[] {_emptyFieldValidationError});
            if (!IsOnlyNumbers()) return (false, new[] {_invalidFieldValidationError});

            return (true, Array.Empty<string>());
        }
    }
}