﻿using System.Collections.Generic;

namespace Entities.Common.Interfaces
{
    public interface IValidatable
    {
        (bool isValid, IEnumerable<string> errors) Validate();
    }
}