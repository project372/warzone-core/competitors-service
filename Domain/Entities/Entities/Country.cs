﻿using System.Collections.Generic;
using System.Linq;
using Entities.Base;
using Entities.Common;
using Entities.Common.Validation;

namespace Entities.Entities
{
    public class Country : BaseEntity
    {
        public string Name { get; set; }
        public int LegalAge { get; set; }
        public override (bool isValid, IEnumerable<string> errors) Validate()
        {
            var errors = new List<string>();
            
            if(string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name)) errors.Add(CountryValidation.EmptyCountryName);
            if(LegalAge == default) errors.Add(CountryValidation.EmptyLegalAge);

            return (!errors.Any(), errors);
        }
    }
}