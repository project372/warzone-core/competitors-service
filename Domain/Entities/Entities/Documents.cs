﻿using System;
using System.Collections.Generic;
using Entities.Base;
using Entities.Common;

namespace Entities.Entities
{
    public class Documents : BaseEntity
    {
        public string IdentityPath { get; set; }
        public string ProofOfResidencePath { get; set; }
        public string ProofOfBankingAcoountPath { get; set; }
        public string SelfiePath { get; set; }

        public bool HasSentAllDocuments() =>
            !(string.IsNullOrEmpty(IdentityPath) || string.IsNullOrEmpty(ProofOfResidencePath) ||
              string.IsNullOrEmpty(ProofOfBankingAcoountPath) || string.IsNullOrEmpty(SelfiePath));

        public override (bool isValid, IEnumerable<string> errors) Validate() => 
            (true, Array.Empty<string>());
    }
}