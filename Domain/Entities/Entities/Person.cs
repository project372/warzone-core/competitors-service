﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Base;
using Entities.Common;
using Entities.Common.Validation;
using Entities.ValueObjects;
using Entities.ValueObjects.Person;

namespace Entities.Entities
{
    public class Person : BaseEntity
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public CountryCode CountryCode { get; set; } = new();
        public AreaCode AreaCode { get; set; } = new();
        public PhoneNumber PhoneNumber { get; set; } = new();
        public Email Email { get; set; } = new();
        public bool IsEmailConfirmed { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public ZipCode ZipCode { get; set; } = new();

        public IdentityNumber IdentityNumber { get; set; } = new();
        public Cpf CpfNumber { get; set; } = new();

        public int CountryId { get; set; }
        public Country Country { get; set; }

        public int? DocumentsId { get; set; }
        public Documents Documents { get; set; }

        public int? ResponsiblePersonId { get; set; }
        public Person ResponsiblePerson { get; set; }

        public int? BankAccountId { get; set; }
        public BankAccount BankAccount { get; set; }


        public bool IsOfAge(DateTime now) => now.AddYears(-Country.LegalAge) >= BirthDate;

        public override (bool isValid, IEnumerable<string> errors) Validate()
        {
            var errors = new List<string>();

            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrWhiteSpace(FirstName)) errors.Add(PersonValidation.EmptyFirstName);
            if (string.IsNullOrEmpty(LastName) || string.IsNullOrWhiteSpace(LastName)) errors.Add(PersonValidation.EmptyLastName);
            errors.AddRange(CountryCode.Validate().errors);
            errors.AddRange(AreaCode.Validate().errors);
            errors.AddRange(PhoneNumber.Validate().errors);
            errors.AddRange(Email.Validate().errors);
            if (string.IsNullOrEmpty(Address) || string.IsNullOrWhiteSpace(Address)) errors.Add(PersonValidation.EmptyAddress);
            if (string.IsNullOrEmpty(State) || string.IsNullOrWhiteSpace(State)) errors.Add(PersonValidation.EmptyState);
            errors.AddRange(ZipCode.Validate().errors);
            errors.AddRange(IdentityNumber.Validate().errors);
            errors.AddRange(CpfNumber.Validate().errors);
            if (CountryId == default) errors.Add(PersonValidation.EmptyCountry);

            return (!errors.Any(), errors);
        }

        public override bool Equals(object? obj)
        {
            if (obj is not Person otherPerson) return false;
            return GetHashCode() == otherPerson.GetHashCode() && Equals(otherPerson);
        }

        private bool Equals(Person other)
        {
            return FirstName == other.FirstName && 
                   MiddleName == other.MiddleName && 
                   LastName == other.LastName && 
                   CountryCode == other.CountryCode && 
                   AreaCode == other.AreaCode && 
                   PhoneNumber == other.PhoneNumber && 
                   Email == other.Email && 
                   IsEmailConfirmed == other.IsEmailConfirmed && 
                   BirthDate == other.BirthDate && 
                   Address == other.Address && 
                   State == other.State && 
                   ZipCode == other.ZipCode && 
                   IdentityNumber == other.IdentityNumber && 
                   CpfNumber == other.CpfNumber && 
                   CountryId == other.CountryId && 
                   Country == other.Country && 
                   DocumentsId == other.DocumentsId && 
                   Documents == other.Documents && 
                   ResponsiblePersonId == other.ResponsiblePersonId &&
                   BankAccountId == other.BankAccountId && 
                   BankAccount.Equals(other.BankAccount);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(Id);
            hashCode.Add(CpfNumber.Value);
            hashCode.Add(Email.Value);
            
            return hashCode.ToHashCode();
        }
    }
}