﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Base;
using Entities.Common;
using Entities.Common.Validation;
using Entities.Enums;
using Entities.ValueObjects.BankAccount;

namespace Entities.Entities
{
    public class BankAccount : BaseEntity
    {
        public string BankName { get; set; }
        public BankAgency BankAgency { get; set; } = new();
        public AccountNumber AccountNumber { get; set; } = new();
        public AccountDigit AccountDigit { get; set; } = new();
        public string OperationIdentifier { get; set; }
        public AccountType AccountType { get; set; }

        public override (bool isValid, IEnumerable<string> errors) Validate()
        {
            var errors = new List<string>();

            if (string.IsNullOrEmpty(BankName) || string.IsNullOrWhiteSpace(BankName)) errors.Add(BankAccountValidation.EmptyBankName);
            errors.AddRange(BankAgency.Validate().errors);
            errors.AddRange(AccountDigit.Validate().errors);
            errors.AddRange(AccountNumber.Validate().errors);

            return (!errors.Any(), errors);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(Id);
            hashCode.Add(AccountNumber);

            return hashCode.ToHashCode();
        }

        public override bool Equals(object? obj)
        {
            if (obj is not BankAccount otherAccount) return false;
            return GetHashCode() == obj.GetHashCode() && Equals(otherAccount);
        }

        public bool Equals(BankAccount bankAccount) =>
            BankAgency == bankAccount.BankAgency &&
            BankName == bankAccount.BankName &&
            AccountNumber == bankAccount.AccountNumber &&
            AccountDigit == bankAccount.AccountDigit &&
            OperationIdentifier == bankAccount.OperationIdentifier &&
            AccountType == bankAccount.AccountType;
    }
}