﻿namespace Entities.Enums
{
    public enum AccountType
    {
        Checking,
        Savings
    }
}