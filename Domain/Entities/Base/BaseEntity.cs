﻿using System;
using System.Collections.Generic;
using Entities.Common.Interfaces;

namespace Entities.Base
{
    public abstract class BaseEntity : IValidatable
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public abstract (bool isValid, IEnumerable<string> errors) Validate();
    }
}