﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.BankAccount
{
    public record BankAgency : RequiredOnlyNumbersString
    {
        public BankAgency() : base(BankAccountValidation.EmptyBankAgency, BankAccountValidation.InvalidBankAgency)
        {
            
        }
    }
}