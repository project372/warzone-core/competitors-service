﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.BankAccount
{
    public record AccountNumber : RequiredOnlyNumbersString
    {
        public AccountNumber() : base(BankAccountValidation.EmptyAccountNumber, BankAccountValidation.InvalidAccountNumber)
        {
        }
    }
}