﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.BankAccount
{
    public record AccountDigit : RequiredOnlyNumbersString
    {
        public AccountDigit() : base(BankAccountValidation.EmptyAccountDigit, BankAccountValidation.InvalidAccountDigit)
        {
        }
    }
}