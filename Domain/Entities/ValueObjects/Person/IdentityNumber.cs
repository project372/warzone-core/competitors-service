﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record IdentityNumber : RequiredOnlyNumbersString
    {
        public IdentityNumber() : base(PersonValidation.EmptyIdentityNumber, PersonValidation.InvalidIdentityNumber)
        {
            
        }
    }
}