﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record PhoneNumber : RequiredOnlyNumbersString
    {
        public PhoneNumber() : base(PersonValidation.EmptyPhoneNumber, PersonValidation.InvalidPhoneNumber)
        {
        }
    }
}