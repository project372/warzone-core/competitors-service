﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record ZipCode : RequiredOnlyNumbersString
    {
        public ZipCode() : base(PersonValidation.EmptyZipCode, PersonValidation.InvalidZipCode)
        {
        }
    }
}