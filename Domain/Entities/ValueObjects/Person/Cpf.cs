﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Common;
using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record Cpf : RequiredOnlyNumbersString
    {

        public Cpf() : base(PersonValidation.EmptyCpf, PersonValidation.InvalidCpf)
        {
            
        }
        public override (bool isValid, IEnumerable<string> errors) Validate()
        {
            var initialValidation = base.Validate();
            if (!initialValidation.isValid) return initialValidation;
            
            if (Value.Length != 11) return (false, new[] {PersonValidation.InvalidCpf,});

            var firstNumber = Value.First();
            if (Value.All(x => x == firstNumber)) return (false, new[] {PersonValidation.InvalidCpf,});

            var numbers = GetAsNumberEnumerable();
            var sum01 = GetCpfSum(10, numbers);
            var sum02 = GetCpfSum(11, numbers);
            
            if (!(CalculateVerifyingValue(sum01) == numbers[^2] && CalculateVerifyingValue(sum02) == numbers[^1])) 
                return (false, new[] {PersonValidation.InvalidCpf,});

            return (true, Array.Empty<string>());
        }
        
        private static int GetCpfSum(int startWeight, int[] numbers)
        {
            var sum = 0;
            const int finalWeight = 2;
            for (var i = startWeight; i >= finalWeight; i--)
            {
                var index = startWeight - i;
                sum += numbers[index] * i;
            }

            return sum;
        }

        private static int CalculateVerifyingValue(int sum)
        {
            var verifyingValue = sum * 10 % 11;
            return verifyingValue == 10 ? 0 : verifyingValue;
        }


    }
    

}