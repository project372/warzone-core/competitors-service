﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record AreaCode : RequiredOnlyNumbersString
    {
        public AreaCode() : base(PersonValidation.EmptyAreaCode, PersonValidation.InvalidAreaCode)
        {
        }
    }
}