﻿using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record CountryCode : RequiredOnlyNumbersString
    {
        public CountryCode() : base(PersonValidation.EmptyCountryCode, PersonValidation.InvalidCountryCode)
        {
            
        }
    }
}