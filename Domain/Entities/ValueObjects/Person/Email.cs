﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Entities.Common;
using Entities.Common.Interfaces;
using Entities.Common.Validation;

namespace Entities.ValueObjects.Person
{
    public record Email : IValidatable
    {
        public string Value { get; init; }
        public (bool isValid, IEnumerable<String> errors) Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return (false, new[] {PersonValidation.EmptyEmail,});

            var emailRegex = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"+
                @"@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

            if (!Regex.IsMatch(Value, emailRegex, RegexOptions.IgnoreCase)) return (false, new[] {PersonValidation.InvalidEmail,});

            return (true, Array.Empty<string>());
        }
    }
}