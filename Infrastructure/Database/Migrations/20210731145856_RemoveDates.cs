﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class RemoveDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "DeletionDate",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "DeletionDate",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Countries");

            migrationBuilder.DropColumn(
                name: "DeletionDate",
                table: "Countries");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Countries");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "BankAccounts");

            migrationBuilder.DropColumn(
                name: "DeletionDate",
                table: "BankAccounts");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "BankAccounts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Persons",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionDate",
                table: "Persons",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Persons",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Documents",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionDate",
                table: "Documents",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Documents",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Countries",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionDate",
                table: "Countries",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Countries",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "BankAccounts",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionDate",
                table: "BankAccounts",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "BankAccounts",
                type: "datetime2",
                nullable: true);
        }
    }
}
