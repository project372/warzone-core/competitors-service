﻿using System;
using Cqrs.Infrastructure.Database;
using Logging.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Database
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddRemoteDatabase(this IServiceCollection serviceCollection, string connectionString)
        {
            serviceCollection.AddDbContext<Context>(options => { options.UseSqlServer(connectionString); });

            AddAbstractions(serviceCollection);

            using var serviceProvider = serviceCollection.BuildServiceProvider();
            using var context = serviceProvider.GetRequiredService<Context>();

            if (!context.Database.CanConnect())
            {
                throw new InvalidOperationException("Unable to connect database.");
            }

            return serviceCollection;
        }

        private static void AddAbstractions(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddScoped<IContext>(sp => sp.GetRequiredService<Context>())
                .AddScoped<ILoggingContext>(sp => sp.GetRequiredService<Context>());
        }

        public static IServiceCollection AddLocalDatabase(this IServiceCollection serviceCollection, string databasePath)
        {
            serviceCollection.AddDbContext<Context>(options =>
            {
                options.UseSqlite($"Data Source={databasePath};Foreign Keys = False");
            });

            AddAbstractions(serviceCollection);

            using var serviceProvider = serviceCollection.BuildServiceProvider();
            using var context = serviceProvider.GetRequiredService<Context>();
            context.Database.EnsureCreated();

            return serviceCollection;
        }
    }
}