﻿using System.Threading.Tasks;
using Cqrs.Infrastructure.Database;
using Database.Configurations;
using Entities.Entities;
using Logging.Entities;
using Logging.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Database
{
    internal class Context : DbContext, IContext, ILoggingContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Documents> Documents { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<LogEntry> Logs { get; set; }

        public Context()
        {
        }

        public Context(DbContextOptions options) : base(options)
        {
        }

        public Task<int> SaveChangesAsync() => SaveChangesAsync(default);

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;
            
            var assemblyPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var pathWithoutFilePrefix = assemblyPath.Substring(6);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(pathWithoutFilePrefix)
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("Database"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new PersonConfiguration())
                .ApplyConfiguration(new BankAccountConfiguration())
                .ApplyConfiguration(new CountryConfiguration());
        }
    }
}