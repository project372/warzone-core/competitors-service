﻿using Entities.Entities;
using Entities.ValueObjects.BankAccount;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations
{
    internal class BankAccountConfiguration : IEntityTypeConfiguration<BankAccount>
    {
        public void Configure(EntityTypeBuilder<BankAccount> builder)
        {
            builder.Property(x => x.BankName).IsRequired();
            builder.Property(x => x.BankAgency)
                .IsRequired()
                .HasConversion(x => x.Value, y => new BankAgency {Value = y});
            builder.Property(x => x.AccountNumber)
                .IsRequired()
                .HasConversion(x => x.Value, y => new AccountNumber {Value = y});
            builder.Property(x => x.AccountDigit)
                .IsRequired()
                .HasConversion(x => x.Value, y => new AccountDigit {Value = y});
            builder.HasIndex(x => x.AccountNumber).IsUnique();
        }
    }
}