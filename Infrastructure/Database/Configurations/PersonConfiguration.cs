﻿using Entities.Entities;
using Entities.ValueObjects;
using Entities.ValueObjects.Person;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations
{
    internal class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x => x.LastName).IsRequired();
            builder.Property(x => x.CountryCode)
                .IsRequired()
                .HasConversion(x => x.Value,
                    y => new CountryCode {Value = y});
            builder.Property(x => x.AreaCode)
                .IsRequired()
                .HasConversion(x => x.Value, y => new AreaCode {Value = y});
            builder.Property(x => x.PhoneNumber)
                .IsRequired()
                .HasConversion(x => x.Value, y => new PhoneNumber {Value = y});
            builder.Property(x => x.Email)
                .IsRequired()
                .HasConversion(x => x.Value, y => new Email {Value = y});
            builder.HasIndex(x => x.Email).IsUnique();
            builder.Property(x => x.Address).IsRequired();
            builder.Property(x => x.State).IsRequired();
            builder.Property(x => x.ZipCode)
                .IsRequired()
                .HasConversion(x => x.Value, y => new ZipCode {Value = y});
            builder.Property(x => x.IdentityNumber)
                .IsRequired()
                .HasConversion(x => x.Value, y => new IdentityNumber {Value = y});
            builder.HasIndex(x => x.IdentityNumber).IsUnique();
            builder.Property(x => x.CpfNumber)
                .IsRequired()
                .HasConversion(x => x.Value, y=> new Cpf{Value = y});
            builder.HasIndex(x => x.CpfNumber).IsUnique();
        }
    }
}