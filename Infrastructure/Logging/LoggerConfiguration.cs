﻿using Microsoft.Extensions.Logging;

namespace Logging
{
    internal class LoggerConfiguration
    {
        public LogLevel LogLevel { get; set; }
    }
}