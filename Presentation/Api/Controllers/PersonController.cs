﻿using System;
using System.Threading.Tasks;
using Api.Base.Controllers;
using Api.Common;
using Api.RequestModels;
using Cqrs.Commands.Interfaces.Person;
using Cqrs.Common;
using Entities.Entities;
using Logging;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : CustomController
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<PersonController> _logger;

        public PersonController(IServiceProvider serviceProvider, IStringLocalizer<Globalization.Globalization> localizer, 
            ILogger<PersonController>? logger) : base(localizer)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        [HttpPost]
        public async Task<Response> AddAsync(AddOrUpdatePersonRequest personRequest)
        {
            try
            {
                var culture = Request
                    .HttpContext
                    .Features
                    .Get<IRequestCultureFeature>()
                    .RequestCulture.
                    Culture;
                
                var command = _serviceProvider.GetRequiredService<ISavePersonCommand>();
                var result = await command.InvokeAsync(personRequest.ToPerson(culture));
                if (result is not Result<Person> successResult) return MakeNonSuccessfulResponse(result);
                return new Response<Person>
                {
                    ResponseData = successResult.Data
                };
            }
            catch (Exception e)
            {
                _logger?.LogStructuredException(e, personRequest);
                return MakeInternalErrorResponse();
            }
        }
    }
}