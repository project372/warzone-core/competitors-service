﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Cqrs.Common;
using Cqrs.Common.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Api.Base.Controllers
{
    public abstract class CustomController : ControllerBase
    {
        public const string InternalErrorDefaultMessage = nameof(InternalErrorDefaultMessage);
        public const string NotFoundDefaultMessage = nameof(NotFoundDefaultMessage);
        
        public readonly IStringLocalizer<Globalization.Globalization> Localizer;

        protected CustomController(IStringLocalizer<Globalization.Globalization> localizer)
        {
            Localizer = localizer;
        }

        protected Response MakeInternalErrorResponse()
        {
            HttpContext.Response.StatusCode = 500;
            return new Response
            {
                IsSuccessful = false,
                Messages = new[]
                {
                    Localizer[InternalErrorDefaultMessage].Value
                }
            };
        }

        protected Response MakeNonSuccessfulResponse(Result result) => result.Type switch
        {
            ResultType.NotFound => MakeNotFoundResponse(result),
            ResultType.InternalError => MakeInternalErrorResponse(),
            ResultType.NotValid => MakeNotValidErrorResponse(result),
            _ => throw new InvalidOperationException($"Invalid result type at {nameof(MakeNonSuccessfulResponse)} ")
        };

        private Response MakeNotValidErrorResponse(Result result)
        {
            HttpContext.Response.StatusCode = 400;
            return new Response
            {
                IsSuccessful = false,
                Messages = result.ValidationErrors.Select(x => Localizer[x].Value)
            };
        }

        private Response MakeNotFoundResponse(Result result)
        {
            HttpContext.Response.StatusCode = 404;
            return new Response
            {
                IsSuccessful = false,
                Messages = new[]
                {
                    Localizer[NotFoundDefaultMessage].Value
                }
            };
        }
    }
}