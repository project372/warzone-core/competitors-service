﻿using System;
using System.Globalization;
using Entities.Entities;
using Entities.Enums;
using Entities.ValueObjects.BankAccount;
using Entities.ValueObjects.Person;

namespace Api.RequestModels
{
    public class AddOrUpdatePersonRequest
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string BirthDate { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string IdentityNumber { get; set; }
        public string CpfNumber { get; set; }
        public int CountryId { get; set; }
        
        public string BankName { get; set; }
        public string BankAgency { get; set; }
        public string AccountNumber { get; set; }
        public string AccountDigit { get; set; }
        public string OperationIdentifier { get; set; }
        public string AccountType { get; set; }

        public Person ToPerson(CultureInfo currentCulture) => new()
        {
            Address = Address,
            CountryCode = new CountryCode {Value = CountryCode},
            Email = new Email {Value = Email},
            State = State,
            AreaCode = new AreaCode {Value = AreaCode},
            BankAccount = new BankAccount
            {
                AccountDigit = new AccountDigit {Value = AccountDigit},
                AccountNumber = new AccountNumber {Value = AccountNumber},
                AccountType = Enum.Parse<AccountType>(AccountType, true),
                BankAgency = new BankAgency {Value = BankAgency},
                BankName = BankName,
                OperationIdentifier = OperationIdentifier
            },
            BirthDate = DateTime.Parse(BirthDate, currentCulture),
            CpfNumber = new Cpf {Value = CpfNumber},
            FirstName = FirstName,
            LastName = LastName,
            MiddleName = MiddleName,
            PhoneNumber = new PhoneNumber {Value = PhoneNumber},
            ZipCode = new ZipCode {Value = ZipCode},
            IdentityNumber = new IdentityNumber {Value = IdentityNumber},
            CountryId = CountryId
        };
    }
}