﻿using System.Collections.Generic;

namespace Api.Common
{
    public record Response
    {
        public bool IsSuccessful { get; init; } = true;
        public IEnumerable<string> Messages { get; init; } = new List<string>();
    }

    public record Response<TData> : Response 
    {
        public TData ResponseData { get; init; }
    }
}