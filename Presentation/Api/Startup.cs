using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Cqrs;
using Database;
using Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api", Version = "v1" });
            });
            var connectionString = Configuration.GetConnectionString("Database");
            
            services
                .AddRemoteDatabase(connectionString)
                .AddCommandsAndQueries()
                .AddControllers();
            
            services.AddLogging(builder =>
            {
                builder
                    .ClearProviders()
                    .AddConsole()
                    .AddCustomLoggerProvider();
            });

            services.AddLocalization(x => x.ResourcesPath = "Globalization");
            services.Configure<RequestLocalizationOptions>(x =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("pt-br"),
                    new CultureInfo("en")
                };

                x.DefaultRequestCulture = new RequestCulture(culture: "pt-br", uiCulture: "pt-br");
                x.SupportedCultures = supportedCultures;
                x.SupportedUICultures = supportedCultures;
                x.RequestCultureProviders = new IRequestCultureProvider[] {new AcceptLanguageHeaderRequestCultureProvider()};
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));
            }

            var localizationOptions = app.ApplicationServices.GetRequiredService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(localizationOptions.Value);
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
