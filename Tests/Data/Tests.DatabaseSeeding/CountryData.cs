﻿using Entities.Entities;

namespace Tests.DatabaseSeeding
{
    public static class CountryData
    {
        public static Country GetValidCountry() => new()
        {
            Name = "Brasil",
            LegalAge = 18
        };
    }
}