﻿using Entities.Entities;
using Entities.Enums;
using Entities.ValueObjects.BankAccount;
using Entities.ValueObjects.Person;

namespace Tests.DatabaseSeeding
{
    public static class PersonData
    {
        public static Person GetValidPerson() => new Entities.Entities.Person
            {
                FirstName = "Larry",
                MiddleName = "Silver",
                LastName = "Racoon",
                CountryCode = new CountryCode {Value = "55"},
                AreaCode = new AreaCode {Value = "62"},
                PhoneNumber = new PhoneNumber {Value = "992457844"},
                Email = new Email {Value = "silver_Racoon@test.com"},
                Address = "Don't know",
                State = "Big River of North",
                ZipCode = new ZipCode {Value = "89558012"},
                IdentityNumber = new IdentityNumber {Value = "5889647561"},
                CpfNumber = new Cpf {Value = "89422754003"},
                CountryId = 2,
                BankAccount = new BankAccount
                {
                    AccountDigit = new AccountDigit {Value = "4"},
                    AccountNumber = new AccountNumber {Value = "1234"},
                    AccountType = AccountType.Checking,
                    BankAgency = new BankAgency {Value = "4002"},
                    BankName = "Test"
                }
            };
    }
}