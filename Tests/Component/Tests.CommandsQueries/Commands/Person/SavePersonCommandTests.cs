﻿using System.Linq;
using System.Threading.Tasks;
using Cqrs.Commands.Interfaces.Person;
using Cqrs.Common;
using Cqrs.Common.Enums;
using Cqrs.Infrastructure.Database;
using Entities.Common.Validation;
using Entities.ValueObjects.Person;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Tests.CommandsQueries.Common.Base;
using Tests.CommandsQueries.Mocks.Infrastructure.Database;
using Tests.DatabaseSeeding;
using Xunit;

namespace Tests.CommandsQueries.Commands.Person
{
    public class SavePersonCommandTests : SqlLiteDatabaseTests
    {
        [Fact]
        public async Task InvokeAsyncShouldReturnNotValidResultGivenInvalidEntity()
        {
            var invalidPerson = new Entities.Entities.Person
            {
                FirstName = "Invalid",
                LastName = "Dude",
                CountryCode = new CountryCode {Value = "55"},
                AreaCode = new AreaCode {Value = "83"},
                PhoneNumber = new PhoneNumber {Value = "4587 789"},
                Email = new Email {Value = "abc@abc.efg"},
                Address = "Test",
                State = "Test",
                ZipCode = new ZipCode {Value = "77550669"},
                IdentityNumber = new IdentityNumber {Value = "23"},
                CpfNumber = new Cpf {Value = "587945632188"}
            };
            var expectedErros = new[]
            {
                PersonValidation.InvalidPhoneNumber,
                PersonValidation.InvalidCpf,
                PersonValidation.EmptyCountry
            };

            using var serviceScope = GetServiceScope();
            var provider = serviceScope.ServiceProvider;
            var sut = provider.GetRequiredService<ISavePersonCommand>();

            var result = await sut.InvokeAsync(invalidPerson);

            Assert.Equal(ResultType.NotValid, result.Type);
            Assert.Equal(expectedErros.Length, result.ValidationErrors.Count());
            Assert.True(expectedErros.All(x => result.ValidationErrors.Contains(x)));
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnInternalErrorResultGivenException()
        {
            Collection.RemoveAll<IContext>();
            Collection.AddScoped<IContext, ExceptionContext>();
            var provider = Collection.BuildServiceProvider();

            var validPerson = PersonData.GetValidPerson();

            var sut = provider.GetRequiredService<ISavePersonCommand>();
            var result = await sut.InvokeAsync(validPerson);
            
            Assert.Equal(ResultType.InternalError, result.Type);
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnSuccessResultGivenValidPerson()
        {
            var validPerson = PersonData.GetValidPerson();
            using var actionScope = GetServiceScope();
            var actionProvider = actionScope.ServiceProvider;

            var sut = actionProvider.GetRequiredService<ISavePersonCommand>();
            var result = await sut.InvokeAsync(validPerson);
            var successResult = (Result<Entities.Entities.Person>) result;

            using var assertionScope = GetServiceScope();
            var assertionProvider = assertionScope.ServiceProvider;
            
            var context = assertionProvider.GetRequiredService<IContext>();

            var insertedPerson = await context.Persons.Include(x => x.BankAccount).FirstOrDefaultAsync();
            
            Assert.Equal(ResultType.Success, result.Type);
            Assert.Equal(insertedPerson, successResult.Data);
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnNotValidGivenExistingEmailCpfAndIdentityNumber()
        {
            using var scope = GetServiceScope();

            var expectedErrors = new[]
            {
                PersonValidation.RepeatedCpf,
                PersonValidation.RepeatedEmail,
                PersonValidation.RepeatedIdentity,
            };
            var provider = scope.ServiceProvider;

            var sut = provider.GetRequiredService<ISavePersonCommand>();
            var firstInsertionResult = await sut.InvokeAsync(PersonData.GetValidPerson());
            var secondInsertionResult = await sut.InvokeAsync(PersonData.GetValidPerson());
            
            Assert.Equal(ResultType.Success, firstInsertionResult.Type);
            Assert.Equal(ResultType.NotValid, secondInsertionResult.Type);
            Assert.Equal(expectedErrors.Length, secondInsertionResult.ValidationErrors.Count());
            Assert.True(expectedErrors.All(x => secondInsertionResult.ValidationErrors.Contains(x)));
        }

        [Fact]
        public async Task InvokeAsyncShouldUpdateRecordGivenExistingPerson()
        {
            const string editedName = "Edited";
            var person = PersonData.GetValidPerson();
            Result result;
            
            using (var scope = GetServiceScope())
            {
                var provider = scope.ServiceProvider;
                var sut = provider.GetRequiredService<ISavePersonCommand>();
                result = await sut.InvokeAsync(person);
                var successResult = (Result<Entities.Entities.Person>) result;
                person = successResult.Data;
            }

            person.FirstName = editedName;
            
            using (var scope = GetServiceScope())
            {
                var provider = scope.ServiceProvider;
                var sut = provider.GetRequiredService<ISavePersonCommand>();
                result = await sut.InvokeAsync(person);
            }

            using (var scope = GetServiceScope())
            {
                var provider = scope.ServiceProvider;
                var context = provider.GetRequiredService<IContext>();
                var personAtDatabase = await context.Persons.Include(x => x.BankAccount).FirstAsync();
                
                Assert.Equal(ResultType.Success, result.Type);
                Assert.Equal(1, await context.Persons.CountAsync());
                Assert.Equal(editedName, person.FirstName);
                Assert.Equal(person, personAtDatabase);
            }
            
        }


    }
}