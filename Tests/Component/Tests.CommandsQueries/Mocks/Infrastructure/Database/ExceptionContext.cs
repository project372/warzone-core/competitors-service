﻿using System;
using System.Threading.Tasks;
using Cqrs.Infrastructure.Database;
using Entities.Entities;
using Logging.Entities;
using Logging.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace Tests.CommandsQueries.Mocks.Infrastructure.Database
{
    public class ExceptionContext : IContext, ILoggingContext
    {
        public void Dispose()
        {
            throw new Exception();
        }

        public DbSet<Person> Persons
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public DbSet<Documents> Documents
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public DbSet<BankAccount> BankAccounts
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public DbSet<Country> Countries
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public DbSet<LogEntry> Logs
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        Task<int> IContext.SaveChangesAsync()
        {
            throw new Exception();
        }

        int ILoggingContext.SaveChanges()
        {
            throw new Exception();
        }

        Task<int> ILoggingContext.SaveChangesAsync()
        {
            throw new Exception();
        }

        int IContext.SaveChanges()
        {
            throw new Exception();
        }
    }
}