﻿using System.Threading.Tasks;
using Cqrs.Commands.Interfaces.Person;
using Cqrs.Common;
using Cqrs.Common.Enums;
using Cqrs.Queries.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Tests.CommandsQueries.Common.Base;
using Tests.DatabaseSeeding;
using Xunit;

namespace Tests.CommandsQueries.Queries.Person
{
    public class GetPersonQuery : SqlLiteDatabaseTests
    {

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(87)]
        public async Task InvokeAsyncShouldReturnNotFoundGivenNonExistingId(int id)
        {
            using var scope = GetServiceScope();
            var provider = scope.ServiceProvider;

            var sut = provider.GetRequiredService<IGetPersonQuery>();
            var result = await sut.InvokeAsync(id);
            
            Assert.Equal(ResultType.NotFound, result.Type);
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnNotFoundGivenNonExitingCpf()
        {
            using var scope = GetServiceScope();
            var provider = scope.ServiceProvider;

            var sut = provider.GetRequiredService<IGetPersonQuery>();
            var result = await sut.InvokeAsync("41686860005");
            
            Assert.Equal(ResultType.NotFound, result.Type);
            Assert.Equal(ResultType.NotFound, result.Type);
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnPersonGivenExistingId()
        {
            using var scope = GetServiceScope();
            var provider = scope.ServiceProvider;

            var createPersonCommand = provider.GetRequiredService<ISavePersonCommand>();
            var createResult = await createPersonCommand.InvokeAsync(PersonData.GetValidPerson());
            var successCreateResult = (Result<Entities.Entities.Person>) createResult;

            using var sutScope = GetServiceScope();
            var sutProvider = scope.ServiceProvider;

            var sut = sutProvider.GetRequiredService<IGetPersonQuery>();
            var sutResult = await sut.InvokeAsync(successCreateResult.Data.Id);
            var successSutResult = (Result<Entities.Entities.Person>) sutResult;

            Assert.NotNull(successSutResult.Data);
            Assert.Equal(successCreateResult.Data,successSutResult.Data);
        }

        [Fact]
        public async Task InvokeAsyncShouldReturnPersonGivingExistingCpf()
        {
            using var scope = GetServiceScope();
            var provider = scope.ServiceProvider;

            var createPersonCommand = provider.GetRequiredService<ISavePersonCommand>();
            var createResult = await createPersonCommand.InvokeAsync(PersonData.GetValidPerson());
            var successCreateResult = (Result<Entities.Entities.Person>) createResult;

            using var sutScope = GetServiceScope();
            var sutProvider = scope.ServiceProvider;

            var sut = sutProvider.GetRequiredService<IGetPersonQuery>();
            var sutResult = await sut.InvokeAsync(successCreateResult.Data.CpfNumber.Value);
            var successSutResult = (Result<Entities.Entities.Person>) sutResult;

            Assert.NotNull(successSutResult.Data);
            Assert.Equal(successCreateResult.Data,successSutResult.Data);
        }

    }
}