﻿using System;
using System.IO;
using Cqrs;
using Cqrs.Infrastructure.Database;
using Database;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Tests.CommandsQueries.Common.Base
{
    public abstract class SqlLiteDatabaseTests : IDisposable
    {
        private readonly IServiceProvider _provider;
        protected readonly IServiceCollection Collection;
        protected string DatabasePath;

        protected SqlLiteDatabaseTests()
        {
            DatabasePath = Path.Combine(Directory.GetCurrentDirectory(), $"{Guid.NewGuid()}.sqlite");
            Collection = new ServiceCollection()
                .AddLocalDatabase(DatabasePath)
                .AddLogging(options =>
                {
                    options
                        .ClearProviders()
                        .AddConsole();
                })
                .AddCommandsAndQueries();
            _provider = Collection
                .BuildServiceProvider();
        }

        protected IServiceScope GetServiceScope() => _provider.CreateScope();

        public void Dispose()
        {
            if(File.Exists(DatabasePath)) File.Delete(DatabasePath);
        }
    }
}