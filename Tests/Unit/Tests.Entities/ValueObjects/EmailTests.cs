﻿using System.Collections.Generic;
using System.Linq;
using Entities.Common;
using Entities.Common.Validation;
using Entities.Entities;
using Entities.ValueObjects;
using Entities.ValueObjects.Person;
using Xunit;

namespace Tests.Entities.ValueObjects
{
    public class EmailTests
    {
        [Theory]
        [MemberData(nameof(GetInvalidValues))]
        public void ValidateShouldReturnFalse(string email, IEnumerable<string> expectedErrors)
        {
            var sut = new Email
            {
                Value = email
            };
            var validationResult = sut.Validate();
            
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        [Theory]
        [MemberData(nameof(GetValidValues))]
        public void ValidateShouldReturnTrue(string email)
        {
            var sut = new Email
            {
                Value = email
            };
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        public static IEnumerable<object[]> GetInvalidValues()
        {
            yield return new object[] {"uhdgasifdh", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"uhdgasifdh@", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"abnc@asc", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"abnc@123", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"123@abcg", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"123@abcg,com", new[] {PersonValidation.InvalidEmail}};
            yield return new object[] {"", new[] {PersonValidation.EmptyEmail}};
            yield return new object[] {" ", new[] {PersonValidation.EmptyEmail}};
            yield return new object[] {null, new[] {PersonValidation.EmptyEmail}};
        }

        public static IEnumerable<object[]> GetValidValues()
        {
            yield return new object[] {"test@avx.com"};
            yield return new object[] {"test@avx.com.br"};
            yield return new object[] {"123abc@78.com.cn"};
        }
    }
}