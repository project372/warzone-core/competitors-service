﻿using System.Collections.Generic;
using System.Linq;
using Entities.Common;
using Entities.Common.Validation;
using Xunit;

namespace Tests.Entities.ValueObjects
{
    public class RequiredOnlyNumbersStringTests
    {

        [Theory]
        [MemberData(nameof(GetValidValues))]
        public void IsValidShouldReturnTrue(string value)
        {
            var sut = new FakeRequiredOnlyNumbersField
            {
                Value = value
            };
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        [Theory]
        [MemberData(nameof(GetInvalidValues))]
        public void ValidateShouldReturnFalse(string value, IEnumerable<string> expectedErrors)
        {
            var sut = new FakeRequiredOnlyNumbersField
            {
                Value = value
            };
            var validationResult = sut.Validate();
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        public static IEnumerable<object[]> GetInvalidValues()
        {
            yield return new object[] {"abc", new[] {FakeValidationErrors.InvalidFake}};
            yield return new object[] {",@", new[] {FakeValidationErrors.InvalidFake}};
            yield return new object[] {"12a45", new[] {FakeValidationErrors.InvalidFake}};
            yield return new object[] {"a123", new[] {FakeValidationErrors.InvalidFake}};
            yield return new object[] {"145a", new[] {FakeValidationErrors.InvalidFake}};
            yield return new object[] {"", new[] {FakeValidationErrors.EmptyFake}};
            yield return new object[] {" ", new[] {FakeValidationErrors.EmptyFake}};
            yield return new object[] {null, new[] {FakeValidationErrors.EmptyFake}};
        }

        public static IEnumerable<object[]> GetValidValues()
        {
            yield return new object[] {"123456789"};
            yield return new object[] {"012"};
            yield return new object[] {"3457"};
            yield return new object[] {"2"};
        }
    }

    internal record FakeRequiredOnlyNumbersField : RequiredOnlyNumbersString
    {
        public FakeRequiredOnlyNumbersField() : base(FakeValidationErrors.EmptyFake, FakeValidationErrors.InvalidFake)
        {
        }
    }

    internal class FakeValidationErrors
    {
        public static string EmptyFake = "EmptyFake";
        public static string InvalidFake = "InvalidFake";

    }
}