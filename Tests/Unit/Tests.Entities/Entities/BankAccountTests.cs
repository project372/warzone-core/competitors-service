﻿using System.Collections.Generic;
using System.Linq;
using Entities.Common;
using Entities.Common.Validation;
using Entities.Entities;
using Entities.Enums;
using Entities.ValueObjects.BankAccount;
using Xunit;

namespace Tests.Entities.Entities
{
    public class BankAccountTests
    {
        [Theory]
        [MemberData(nameof(GetInvalidData))]
        public void ValidateShouldReturnFalse(BankAccount sut, IEnumerable<string> expectedErrors)
        {
            var validationResult = sut.Validate();
            
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        public static IEnumerable<object[]> GetInvalidData()
        {
            yield return new object[]
            {
                new BankAccount(),
                new[]
                {
                    BankAccountValidation.EmptyAccountDigit,
                    BankAccountValidation.EmptyAccountNumber,
                    BankAccountValidation.EmptyBankAgency,
                    BankAccountValidation.EmptyBankName,
                }
            };

            yield return new object[]
            {
                new BankAccount
                {
                    BankName = " ",
                    BankAgency = new BankAgency {Value = "1234a"},
                    AccountNumber = new AccountNumber {Value = "@_"},
                    AccountDigit = new AccountDigit {Value = " "},
                    AccountType = AccountType.Checking
                },
                new[]
                {
                    BankAccountValidation.EmptyBankName,
                    BankAccountValidation.EmptyAccountDigit,
                    BankAccountValidation.InvalidAccountNumber,
                    BankAccountValidation.InvalidBankAgency,
                }
            };
        }

        [Theory]
        [MemberData(nameof(GetValidData))]
        public void ValidateShouldReturnTrue(BankAccount sut)
        {
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        public static IEnumerable<object[]> GetValidData()
        {
            yield return new object[]
            {
                new BankAccount
                {
                    BankName = "Santander",
                    BankAgency = new BankAgency {Value = "1158"},
                    AccountNumber = new AccountNumber {Value = "68794"},
                    AccountDigit = new AccountDigit {Value = "8"},
                    AccountType = AccountType.Savings
                }
            };
        }
    }
}