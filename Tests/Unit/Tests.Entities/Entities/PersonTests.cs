﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Common;
using Entities.Common.Validation;
using Entities.Entities;
using Entities.ValueObjects;
using Entities.ValueObjects.Person;
using Xunit;

namespace Tests.Entities.Entities
{
    public class PersonTests
    {

        private readonly DateTime _mockedNow = new DateTime(2021, 07, 03);

        [Theory]
        [InlineData(18, 1996,12,8)]
        [InlineData(18, 2000,3,15)]
        [InlineData(16, 2005,7,3)]
        [InlineData(18, 2003,7,3)]
        public void IsOfAgeShouldReturnTrue(int legalAge, int yearOfBirth, int monthOfBirth, int dayOfBirth)
        {
            var sut = GetSutToBirthDateTest(legalAge, yearOfBirth, monthOfBirth, dayOfBirth);
            Assert.True(sut.IsOfAge(_mockedNow));
        }


        [Theory]
        [InlineData(18, 2020, 5, 8)]
        [InlineData(18, 2003, 7, 4)]
        [InlineData(16, 2005, 7, 4)]
        public void IsOfAgeShouldReturnFalse(int legalAge, int yearOfBirth, int monthOfBirth, int dayOfBirth)
        {
            var sut = GetSutToBirthDateTest(legalAge, yearOfBirth, monthOfBirth, dayOfBirth);
            Assert.False(sut.IsOfAge(_mockedNow));
        }
        
        private Person GetSutToBirthDateTest(int legalAge, int yearOfBirth, int monthOfBirth, int dayOfBirth)
        {
            var birthDate = new DateTime(yearOfBirth, monthOfBirth, dayOfBirth);
            var country = new Country
            {
                LegalAge = legalAge,
                Name = Guid.NewGuid().ToString()
            };
            var sut = new Person
            {
                Country = country,
                BirthDate = birthDate
            };
            return sut;
        }


        [Theory]
        [MemberData(nameof(GetInvalidData))]
        public void ValidateShouldReturnFalse(Person sut, IEnumerable<string> expectedErrors)
        {
            var validationResult = sut.Validate();
            
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        public static IEnumerable<object[]> GetInvalidData()
        {
            yield return new object[]
            {
                new Person(),
                new[]
                {
                    PersonValidation.EmptyFirstName,
                    PersonValidation.EmptyLastName,
                    PersonValidation.EmptyCountryCode, 
                    PersonValidation.EmptyAreaCode, 
                    PersonValidation.EmptyPhoneNumber, 
                    PersonValidation.EmptyEmail, 
                    PersonValidation.EmptyAddress, 
                    PersonValidation.EmptyState, 
                    PersonValidation.EmptyZipCode, 
                    PersonValidation.EmptyIdentityNumber, 
                    PersonValidation.EmptyCpf, 
                    PersonValidation.EmptyCountry
                }
            };

            yield return new object[]
            {
                new Person
                {
                    FirstName = "Michael",
                    LastName = "Bluehorse",
                    CountryCode = new CountryCode {Value = "a54"},
                    AreaCode = new AreaCode{Value = "84 "},
                    PhoneNumber = new PhoneNumber {Value = "(11) 5 8987-3238"},
                    Email = new Email {Value = "get@123. "},
                    Address = "Beautiful Horizon",
                    State = "Holy Spirit",
                    ZipCode = new ZipCode {Value = "Zero-Zero-One"},
                    IdentityNumber = new IdentityNumber {Value = "@"},
                    CpfNumber = new Cpf {Value = "00000000000"},
                    CountryId = 14
                },
                new[]
                {
                    PersonValidation.InvalidCountryCode,
                    PersonValidation.InvalidAreaCode, 
                    PersonValidation.InvalidPhoneNumber, 
                    PersonValidation.InvalidEmail, 
                    PersonValidation.InvalidZipCode, 
                    PersonValidation.InvalidIdentityNumber,
                    PersonValidation.InvalidCpf, 
                }
            };
            
            yield return new object[]
            {
                new Person
                {
                    FirstName = " ",
                    LastName = " ",
                    CountryCode = new CountryCode {Value = "a54"},
                    AreaCode = new AreaCode{Value = "84 "},
                    PhoneNumber = new PhoneNumber {Value = "(11) 5 8987-3238"},
                    Email = new Email {Value = "        "},
                    Address = " ",
                    State = " ",
                    ZipCode = new ZipCode {Value = "       "},
                    IdentityNumber = new IdentityNumber {Value = "@"},
                    CpfNumber = new Cpf {Value = "00000000000"},
                    CountryId = 14
                },
                new[]
                {
                    PersonValidation.EmptyFirstName,
                    PersonValidation.EmptyLastName,
                    PersonValidation.EmptyEmail,
                    PersonValidation.EmptyAddress,
                    PersonValidation.EmptyState,
                    PersonValidation.InvalidCountryCode,
                    PersonValidation.InvalidAreaCode, 
                    PersonValidation.InvalidPhoneNumber, 
                    PersonValidation.EmptyZipCode, 
                    PersonValidation.InvalidIdentityNumber,
                    PersonValidation.InvalidCpf, 
                }
            };
        }

        [Theory]
        [MemberData(nameof(GetValidData))]
        public void ValidateShouldReturnTrue(Person sut)
        {
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        public static IEnumerable<object[]> GetValidData()
        {
            yield return new object[]
            {
                new Person
                {
                    FirstName = "Agatha",
                    MiddleName = "Lars",
                    LastName = "Jumpycat",
                    CountryCode = new CountryCode {Value = "55"},
                    AreaCode = new AreaCode {Value = "62"},
                    PhoneNumber = new PhoneNumber {Value = "992457844"},
                    Email = new Email {Value = "lars.jumpy_agatha@test.com"},
                    Address = "Don't know",
                    State = "Sea Big Spider",
                    ZipCode = new ZipCode {Value = "89558012"},
                    IdentityNumber = new IdentityNumber {Value = "5889647561"},
                    CpfNumber = new Cpf {Value = "89422754003"},
                    CountryId = 1
                }
            };
        }
    }
}