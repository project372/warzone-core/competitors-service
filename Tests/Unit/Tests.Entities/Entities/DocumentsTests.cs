﻿using System;
using Entities.Entities;
using Xunit;

namespace Tests.Entities.Entities
{
    public class DocumentsTests
    {

        [Fact]
        public void HasSentAllDocumentsShouldReturnTrueGivenAllPathsFilled()
        {
            var sut = new Documents
            {
                IdentityPath = Guid.NewGuid().ToString(),
                SelfiePath = Guid.NewGuid().ToString(),
                ProofOfResidencePath = Guid.NewGuid().ToString(),
                ProofOfBankingAcoountPath = Guid.NewGuid().ToString()
            };
            
            Assert.True(sut.HasSentAllDocuments());
        }

        [Fact]
        public void HasSentAllDocumentsShouldReturnFalseGivenEmptyIdentityPath()
        {
            var sut = new Documents
            {
                SelfiePath = Guid.NewGuid().ToString(),
                ProofOfResidencePath = Guid.NewGuid().ToString(),
                ProofOfBankingAcoountPath = Guid.NewGuid().ToString()
            };

            Assert.False(sut.HasSentAllDocuments());
        }
        
        [Fact]
        public void HasSentAllDocumentsShouldReturnFalseGivenEmptySelfiePath()
        {
            var sut = new Documents
            {
                IdentityPath = Guid.NewGuid().ToString(),
                ProofOfResidencePath = Guid.NewGuid().ToString(),
                ProofOfBankingAcoountPath = Guid.NewGuid().ToString()
            };

            Assert.False(sut.HasSentAllDocuments());
        }
        
        [Fact]
        public void HasSentAllDocumentsShouldReturnFalseGivenEmptyProofOfResidencePath()
        {
            var sut = new Documents
            {
                IdentityPath = Guid.NewGuid().ToString(),
                SelfiePath = Guid.NewGuid().ToString(),
                ProofOfBankingAcoountPath = Guid.NewGuid().ToString()
            };

            Assert.False(sut.HasSentAllDocuments());
        }
        
        [Fact]
        public void HasSentAllDocumentsShouldReturnFalseGivenEmptyProofOfProofOfBankingAccountPath()
        {
            var sut = new Documents
            {
                IdentityPath = Guid.NewGuid().ToString(),
                SelfiePath = Guid.NewGuid().ToString(),
                ProofOfResidencePath = Guid.NewGuid().ToString()
            };

            Assert.False(sut.HasSentAllDocuments());
        }
    }
}