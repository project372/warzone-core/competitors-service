﻿using System.Collections.Generic;
using System.Linq;
using Entities.Common.Validation;
using Entities.Entities;
using Xunit;

namespace Tests.Entities.Entities
{
    public class CountryTests
    {
        [Theory]
        [MemberData(nameof(GetInvalidData))]
        public void ValidateShouldReturnFalse(Country sut, IEnumerable<string> expectedErrors)
        {
            var validationResult = sut.Validate();
            
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        public static IEnumerable<object[]> GetInvalidData()
        {
            yield return new object[]
            {
                new Country
                {
                    Name = string.Empty
                },
                new[]
                {
                    CountryValidation.EmptyCountryName,
                    CountryValidation.EmptyLegalAge
                }
            };

            yield return new object[]
            {
                new Country
                {
                    Name = " "
                },
                new[]
                {
                    CountryValidation.EmptyCountryName,
                    CountryValidation.EmptyLegalAge
                }
            };

            yield return new object[]
            {
                new Country
                {
                    Name = null,
                    LegalAge = 21
                },
                new[]
                {
                    CountryValidation.EmptyCountryName
                }
            };
        }

        [Theory]
        [MemberData(nameof(GetValidData))]
        public void ValidateShouldReturnTrue(Country sut)
        {
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        public static IEnumerable<object[]> GetValidData()
        {
            yield return new object[]
            {
                new Country
                {
                    Name = "Brasil",
                    LegalAge = 18
                }
            };
        }
    }
}