﻿using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Api;
using Api.Common;
using Api.RequestModels;
using Cqrs.Infrastructure.Database;
using Entities.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Tests.Controllers.Common;
using Tests.DatabaseSeeding;
using Xunit;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Tests.Controllers.Controllers
{
    [Collection(DatabaseRestorationCollectionFixture.CollectionName)]
    public class PersonControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly string _cultureName = "pt-br";

        public PersonControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        private HttpClient GetHttpClient()
        {
            var sut = _factory.CreateClient();
            sut.DefaultRequestHeaders.AcceptLanguage.Clear();
            sut.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue(_cultureName));

            return sut;
        }
        

        [Fact]
        public async Task AddAsyncShouldAddANewPersonToDatabase()
        {
            var country = await InsertCountryDataAsync();
            using var sut = GetHttpClient();
            var validPerson = PersonData.GetValidPerson();
            validPerson.CountryId = country.Id;
            var request = ConvertPersonToAddOrUpdateRequest(validPerson, _cultureName);

            var result = await sut.PostAsJsonAsync("/Person", request, default);
            var body = await result.Content.ReadAsStringAsync();
            result.EnsureSuccessStatusCode();
            var returnedPerson = JsonSerializer.Deserialize<Response<Person>>(body, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            using var serviceScope = _factory.Services.CreateScope();
            var serviceProvider = serviceScope.ServiceProvider;
            var context = serviceProvider.GetRequiredService<IContext>();

            var insertedPerson = await context
                .Persons
                .Include(x => x.BankAccount)
                .FirstOrDefaultAsync(x => x.CpfNumber == validPerson.CpfNumber);
            Assert.NotNull(insertedPerson);
            Assert.Equal(returnedPerson.ResponseData, insertedPerson);
        }

        private async Task<Country> InsertCountryDataAsync()
        {
            using var scope = _factory.Services.CreateScope();
            var provider = scope.ServiceProvider;
            var context = provider.GetRequiredService<IContext>();

            var country = CountryData.GetValidCountry();

            context.Countries.Add(country);
            await context.SaveChangesAsync();

            return country;
        }

        private static AddOrUpdatePersonRequest ConvertPersonToAddOrUpdateRequest(Person validPerson, string cultureName) =>
            new()
            {
                Address = validPerson.Address,
                Email = validPerson.Email.Value,
                State = validPerson.State,
                AccountDigit = validPerson.BankAccount.AccountDigit.Value,
                AccountNumber = validPerson.BankAccount.AccountNumber.Value,
                AccountType = validPerson.BankAccount.AccountType.ToString(),
                AreaCode = validPerson.AreaCode.Value,
                BankAgency = validPerson.BankAccount.BankAgency.Value,
                BankName = validPerson.BankAccount.BankName,
                BirthDate = validPerson.BirthDate.ToString(new CultureInfo(cultureName)),
                CountryCode = validPerson.CountryCode.Value,
                CountryId = validPerson.CountryId,
                CpfNumber = validPerson.CpfNumber.Value,
                FirstName = validPerson.FirstName,
                IdentityNumber = validPerson.IdentityNumber.Value,
                LastName = validPerson.LastName,
                MiddleName = validPerson.MiddleName,
                OperationIdentifier = validPerson.BankAccount.OperationIdentifier,
                PhoneNumber = validPerson.PhoneNumber.Value,
                ZipCode = validPerson.ZipCode.Value
            };
    }
}