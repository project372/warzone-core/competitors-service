﻿using System;
using System.Globalization;
using System.IO;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Tests.Controllers.Common
{
    public class DatabaseRestorationWizard : IDisposable
    {
        private string _snapshotName = string.Empty;
        private string _databaseName = string.Empty;
        private IntegrationTestsOptions _options;
        private IConfiguration _configuration;

        public DatabaseRestorationWizard()
        {
            SetConfiguration();
            CreateDatabaseSnapshot();
        }

        private void CreateDatabaseSnapshot()
        {
            var now = DateTime.Now
                .ToString(CultureInfo.InvariantCulture)
                .Replace("/", "_")
                .Replace(":", "_")
                .Replace(" ", "_");

            _snapshotName = $"IntegrationTests_Competitors_{now}";
            var snapshotFileName = Path.Combine(_options.SnapshotsDirectory, $"snapshot_Competitors_{now}.ss");
            if (_options.UseForwardSlash) snapshotFileName = snapshotFileName.Replace(@"\", "/");
            using var connection = new SqlConnection(_options.ConnectionString);
            _databaseName = connection.Database;

            connection.Open();
            var getLogicalNameQuery = $"DECLARE @logicalName NVARCHAR(MAX); " +
                                      "SELECT @logicalName = f.name " +
                                      "FROM sys.master_files f " +
                                      "INNER JOIN sys.databases d ON d.database_id = f.database_id " +
                                      $"WHERE f.type_desc = 'ROWS' AND d.name = '{_databaseName}'; " +
                                      "SELECT @logicalName";

            using var getLogicalNameCommand = new SqlCommand(getLogicalNameQuery, connection);
            var logicalName = getLogicalNameCommand.ExecuteScalar().ToString();

            var createSnapshotQuery =
                $"CREATE DATABASE {_snapshotName} ON " +
                $"( NAME = {logicalName}, FILENAME = '{snapshotFileName}' ) " +
                $"AS SNAPSHOT OF {_databaseName};";

            using var createSnapshotCommand = new SqlCommand(createSnapshotQuery, connection);

            createSnapshotCommand.ExecuteNonQuery();
        }

        private void SetConfiguration()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            _configuration = new ConfigurationBuilder()
                .SetBasePath(currentDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            _options = _configuration
                .GetSection("IntegrationTests")
                .Get<IntegrationTestsOptions>();
        }



        public void Dispose()
        {
            using var connection = new SqlConnection(_options.ConnectionString);
            var command = $"USE master;" +
                          $"ALTER DATABASE {_databaseName} SET SINGLE_USER WITH ROLLBACK IMMEDIATE;" +
                          $"RESTORE DATABASE {_databaseName} FROM DATABASE_SNAPSHOT = '{_snapshotName}';" +
                          $"ALTER DATABASE {_databaseName} SET MULTI_USER WITH NO_WAIT;" +
                          $"DROP DATABASE {_snapshotName}";

            using var revertSnapshotCommand = new SqlCommand(command, connection);

            connection.Open();
            revertSnapshotCommand.ExecuteNonQuery();
        }
    }
}