﻿using Xunit;

namespace Tests.Controllers.Common
{
    [CollectionDefinition(CollectionName)]
    public class DatabaseRestorationCollectionFixture : ICollectionFixture<DatabaseRestorationWizard>
    {
        public const string  CollectionName = "DatabaseRestorationCollection";
    }
}