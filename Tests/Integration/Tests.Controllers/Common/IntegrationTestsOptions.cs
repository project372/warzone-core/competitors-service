﻿namespace Tests.Controllers.Common
{
    internal class IntegrationTestsOptions
    {
        public string ConnectionString { get; set; }
        public string SnapshotsDirectory { get; set; }
        public bool UseForwardSlash { get; set; }
    }
}